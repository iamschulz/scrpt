// http://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parserOptions: {
        parser: 'babel-eslint',
        sourceType: 'module'
    },
    env: {
        browser: true,
        node: true
    },
    extends: [
        "plugin:vue/recommended",
        "prettier/vue",
        "plugin:prettier/recommended"
    ],
    // required to lint *.vue files
    plugins: [
        'html',
        'prettier'
    ],
    // add your custom rules here
    'rules': {
        // allow paren-less arrow functions
        'arrow-parens': 0,
        // allow async-await
        'generator-star-spacing': 0,
        // allow debugger during development
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
        "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
        'semi': [2, 'always'],
        'one-var': ['error', 'always'],
        "prettier/prettier": ["error", {
            "endOfLine": "auto"
        }],
    }
}
