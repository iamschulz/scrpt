import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

import appStore from "./appStore";
import projectStore from "./projectStore";

Vue.use(Vuex);
const state = {},
    getters = {},
    actions = {},
    mutations = {},
    vuexLocal = new VuexPersistence({
        key: "scrpt",
        storage: window.localStorage
    });

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations,
    modules: {
        app: appStore,
        project: projectStore
    },
    plugins: [vuexLocal.plugin]
});
