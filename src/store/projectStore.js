export default {
    state: {
        project: {
            title: "",
            author: "",
            mail: "",
            phone: "",
            synopsis: ""
        },

        location: [],

        character: [],

        script: []
    },

    getters: {
        getLocationById: state => id => {
            return state.location.find(location => location.id === id);
        },

        getLocationByName: state => name => {
            return state.location.find(location => location.name === name);
        },

        getCharacterById: state => id => {
            return state.character.find(character => character.id === id);
        },

        getCharacterByName: state => name => {
            return state.character.find(character => character.name === name);
        },

        getParagraphById: state => id => {
            return state.script.find(paragraph => paragraph.id === id);
        },

        getRelationCountById: state => (id, type) => {
            if (type) {
                let relsArr = state.script[id].rel[type] || [];
                return relsArr.length;
            } else {
                return Object.keys(state.script[id].rel)
                    .map(types => {
                        const thisType = state.script[id].rel[types];
                        return thisType.length;
                    })
                    .reduce((p, c) => p + c, 0);
            }
        },

        getSceneByParagraphId: state => id => {
            let sceneId = 0;

            if (state.script[id].type === "scene") {
                sceneId = id;
            } else {
                for (let i = id - 1; i >= 0; i--) {
                    if (state.script[i].type === "scene") {
                        sceneId = i;
                        break;
                    }
                }
            }

            return sceneId;
        },

        getSceneNumberByParagraphId: state => id => {
            let scenes = state.script.filter(
                    paragraph => paragraph.type === "scene"
                ),
                sceneNumber = 0;

            for (let i = 0; i <= scenes.length; i++) {
                if (scenes[i].id === id) {
                    sceneNumber = i;
                    break;
                }
            }

            return sceneNumber + 1;
        },

        getRelatedCharacterByParagraph: state => id => {
            let characterParagraphId = 0;

            if (state.script[id].type === "character") {
                characterParagraphId = id;
            } else {
                for (let i = id - 1; i >= 0; i--) {
                    if (state.script[i].type === "character") {
                        characterParagraphId = i;
                        break;
                    }
                }
            }

            return state.script[characterParagraphId].rel.character
                ? state.script[characterParagraphId].rel.character[0]
                : false;
        },

        /**
         * returns a HSL value generated from the name of a specified character
         */
        getCharacterColor: state => id => {
            let hash = function(str) {
                    let hash = 0;

                    for (let i = 0; i < str.length; i++) {
                        hash = str.charCodeAt(i) + ((hash << 5) - hash);
                    }

                    return hash & 0x0168; // 168 = 360 in decimal, color spin is 360 deg
                },
                hue = state.character[id] ? hash(state.character[id].name) : 0,
                sat = state.character[id] ? "100%" : "0%",
                hsl = [hue, sat, "42%"];

            return "hsl(" + hsl.join() + ")";
        }
    },

    actions: {},

    mutations: {
        editProjectTitle(state, payload) {
            state.project.title = payload;
        },

        editProjectSynopsis(state, payload) {
            state.project.synopsis = payload;
        },

        editProjectAuthorName(state, payload) {
            state.project.author = payload;
        },

        editProjectAuthorAddress(state, payload) {
            state.project.address = payload;
        },

        editProjectAuthorMail(state, payload) {
            state.project.mail = payload;
        },

        editProjectAuthorPhone(state, payload) {
            state.project.phone = payload;
        },

        addNewLocation(state, payload) {
            const location = {
                id: state.location.length,
                name: payload.name,
                energy: payload.energy,
                water: payload.water,
                sanitary: payload.sanitary,
                notes: payload.notes,
                images: []
            };

            state.location.push(location);
        },

        editLocation(state, payload) {
            const location = {
                id: payload.id,
                name: payload.name,
                energy: payload.energy,
                water: payload.water,
                sanitary: payload.sanitary,
                notes: payload.notes,
                images: payload.images
            };

            state.location.splice(payload.id, 1, location);
        },

        removeLocation(state, id) {
            let index = state.character.findIndex(
                location => location.id === id
            );
            state.location.splice(index, 1);
        },

        addNewCharacter(state, payload) {
            const character = {
                id: state.character.length,
                name: payload.name,
                avatar: payload.avatar,
                description: payload.description,
                bio: payload.bio,
                age: payload.age,
                height: payload.height,
                weight: payload.weight,
                hair: payload.hair,
                eyes: payload.eyes,
                role: payload.role,
                actor: payload.actor,
                images: []
            };

            state.character.push(character);
        },

        editCharacter(state, payload) {
            const character = {
                id: payload.id,
                name: payload.name,
                avatar: payload.avatar,
                notes: payload.notes,
                description: payload.description,
                bio: payload.bio,
                age: payload.age,
                height: payload.height,
                weight: payload.weight,
                hair: payload.hair,
                eyes: payload.eyes,
                role: payload.role,
                actor: payload.actor,
                images: payload.images
            };

            state.character.splice(payload.id, 1, character);
        },

        removeCharacter(state, id) {
            let index = state.character.findIndex(
                character => character.id === id
            );
            state.character.splice(index, 1);
        },

        addNewParagraph(state, payload) {
            const paragraph = {
                id: payload.id,
                type: payload.type,
                content: payload.content,
                rel: payload.rel
            };

            state.script.splice(payload.id, 0, paragraph);
        },

        editParagraph(state, payload) {
            const paragraph = {
                id: payload.id,
                type: payload.type,
                content: payload.content,
                rel: payload.rel
            };

            state.script.splice(payload.id, 1, paragraph);
        },

        removeParagraph(state, id) {
            let index = state.script.findIndex(
                paragraph => paragraph.id === id
            );
            state.script.splice(index, 1);
        }
    }
};
