export default {
    state: {
        offcanvas: false,
        modal: false
    },

    mutations: {
        toggleOffcanvas(state, payload) {
            state.offcanvas =
                typeof payload === "undefined" ? !state.offcanvas : payload;
        },
        toggleModal(state, payload) {
            state.modal =
                typeof payload === "undefined" ? !state.modal : payload;
        }
    }
};
