import TypeAhead from "type-ahead";
import OffCanvasMenu from "../OffCanvasMenu/OffCanvasMenu.vue";
import SaveLoadFile from "../SaveLoadFile/SaveLoadFile.vue";

export default {
    name: "app",
    components: {
        "offcanvas-menu": OffCanvasMenu,
        "save-load": SaveLoadFile
    },
    methods: {
        /**
         * Initialize autocomplete
         *
         * @param {Node} $el
         * @param {Array} list
         * @param {Function} callbackFn
         */
        autocomplete: function($el, list, callbackFn) {
            // remove old suggestions
            if (
                $el.dataset.autocompleteInit &&
                $el.parentElement.querySelector(".suggestions")
            ) {
                $el.parentElement.querySelector(".suggestions").remove();
            }

            let data = list || $el.dataset.autocomplete.split(","),
                typeaheadEl = new TypeAhead($el, data, {
                    minLength: 0,
                    fulltext: true,
                    onMouseDown: callbackFn,
                    onKeyDown: callbackFn
                });

            typeaheadEl.list.element.classList.add("suggestions");
            typeaheadEl.list.element.classList.add("elevation-1");
            typeaheadEl.list.element.style.display = "block";
            typeaheadEl.list.show = function() {
                this.element.classList.add("is--shown");
            };
            typeaheadEl.list.hide = function() {
                this.element.classList.remove("is--shown");
            };

            $el.dataset.autocompleteInit = "true";
            return typeaheadEl;
        },

        /**
         * Orders the ID of a specified store
         *
         * @param {String} store - Name of the store object
         */
        orderStoreIDs: function(store) {
            for (let i = 0; i < this.$store.state.project[store].length; i++) {
                this.$store.state.project[store][i].id = i;
            }
        },

        /**
         * Enables/Disables scrolling
         *
         * @param {Boolean} scroll
         */
        toggleBodyScroll: function(scroll) {
            document
                .querySelector("body")
                .classList.toggle("is--no-scroll", !scroll);
        }
    },

    computed: {
        bodyScroll() {
            return this.$store.state.app.scroll;
        },

        projectTitle: {
            get() {
                return this.$store.state.project.project.title;
            }
        }
    },

    beforeMount() {
        this.$root = this;
        this.$root.orderStoreIDs("script");
        this.$root.orderStoreIDs("character");
        this.$root.orderStoreIDs("location");

        /**
         * always start with closed menus and modals
         */
        this.$store.commit("toggleOffcanvas", false);
        this.$store.commit("toggleModal", false);
    },

    mounted() {
        /**
         * Bind root references to window for global access
         */
        this.$nextTick(() => {
            window.app.$refs = this.$refs;
        });
    },

    watch: {
        bodyScroll(scroll) {
            this.toggleBodyScroll(scroll);
        }
    }
};
