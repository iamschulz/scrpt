import LinkMenu from "../LinkMenu/LinkMenu.vue";

export default {
    components: {
        "link-menu": LinkMenu
    },

    data: function() {
        return {
            paragraphTypes: [
                {
                    name: "scene",
                    icon: "landscape"
                },
                {
                    name: "action",
                    icon: "format_align_left"
                },
                {
                    name: "character",
                    icon: "person"
                },
                {
                    name: "parenthesis",
                    icon: "live_help"
                },
                {
                    name: "dialog",
                    icon: "chat"
                },
                {
                    name: "shot",
                    icon: "camera"
                }
            ]
        };
    },

    methods: {
        onLinkClick: function() {
            this.$emit("linkClick");
        }
    },

    beforeDestroy: function() {
        this.$store.state.app.modal = false;
    },

    props: {
        id: Number,
        type: String
    }
};
