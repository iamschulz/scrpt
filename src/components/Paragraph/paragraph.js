import CharacterButton from "../CharacterButton/CharacterButton.vue";
import ParagraphHelper from "../ParagraphHelper/ParagraphHelper.vue";

export default {
    components: {
        "character-button": CharacterButton,
        "paragraph-helper": ParagraphHelper
    },

    data: function() {
        return {
            hover: false,
            focus: false,
            lines: 1,
            paragraphTypes: [
                {
                    name: "scene",
                    icon: "landscape",
                    spellcheck: true,
                    autocorrect: false,
                    autocapitalize: true,
                    characters: 60
                },
                {
                    name: "action",
                    icon: "format_align_left",
                    spellcheck: true,
                    autocorrect: true,
                    autocapitalize: true,
                    characters: 60
                },
                {
                    name: "character",
                    icon: "person",
                    spellcheck: false,
                    autocorrect: false,
                    autocapitalize: true,
                    characters: 36
                },
                {
                    name: "parenthesis",
                    icon: "live_help",
                    spellcheck: true,
                    autocorrect: true,
                    autocapitalize: false,
                    characters: 36
                },
                {
                    name: "dialog",
                    icon: "chat",
                    spellcheck: true,
                    autocorrect: true,
                    autocapitalize: true,
                    characters: 36
                },
                {
                    name: "shot",
                    icon: "camera",
                    spellcheck: false,
                    autocorrect: false,
                    autocapitalize: true,
                    characters: 60
                }
            ]
        };
    },

    props: {
        id: Number,
        typeProp: String,
        contentProp: String,
        relProp: Object
    },

    methods: {
        onEnter: function() {
            // add new paragraph on enter, if autosuggest is hidden
            if (!this.$el.querySelector(".suggestions.is--shown")) {
                this.$emit("addNewParagraph", this.id);
            }
        },

        onFocus: function() {
            this.focus = true;

            document.body.addEventListener("click", () => {
                this.unsetFocus();
            });

            document.body.addEventListener("keydown", event => {
                this.unsetFocus();
            });
        },

        unsetFocus: function() {
            if (
                !this.$el.contains(document.activeElement) &&
                this.$el !== document.activeElement
            ) {
                this.focus = false;
                removeEventListener("click", document.body);
                removeEventListener("keydown", document.body);

                // todo: makes v-dialog within paragraph helper act buggy
                // todo: autocomplete doesn't work well with this yet
            }
        },

        onEscape: function() {
            // close autosuggest
            if (this.$el.querySelector(".suggestions.is--shown")) {
                this.autocomplete.candidates = [];
            }
        },

        /**
         * Cycles through or activates paragraph types
         *
         * @param {String} newType
         * @param {Boolean} reverseCycle
         */
        changeParagraphType: function(newType, reverseCycle) {
            let paragraph = this.$store.getters.getParagraphById(this.id);

            // remove old suggestions
            if (
                this.$el.querySelector(".paragraph__input").dataset
                    .autocompleteInit &&
                this.$el.querySelector(".suggestions")
            ) {
                this.$el.querySelector(".suggestions").remove();
            }

            if (!newType) {
                let thisType = this.type,
                    currentPos = this.paragraphTypes.indexOf(
                        this.paragraphTypes.find(type => type.name === thisType)
                    ),
                    newPos = currentPos;

                if (!reverseCycle) {
                    newPos =
                        currentPos < this.paragraphTypes.length - 1
                            ? currentPos + 1
                            : 0;
                } else {
                    newPos =
                        currentPos === 0
                            ? this.paragraphTypes.length - 1
                            : currentPos - 1;
                }
                newType = this.paragraphTypes[newPos].name;
            }

            paragraph.type = newType;

            this.$store.commit("editParagraph", paragraph);
            this.$el.querySelector(".paragraph__input").focus();

            this.$nextTick(() => {
                this.initAutocomplete();
            });
        },

        /**
         * Updates the store with new paragraph content
         *
         * @param {Event} event
         * @param {String} content
         */
        saveContent: function(event, content) {
            // don't trigger, when paragraph blur target is inside another paragraph element
            // Safari doesn't send any related target
            let isInside =
                this.$el.contains(event.relatedTarget) || !event.relatedTarget;

            if (!isInside) {
                let paragraph = this.$store.getters.getParagraphById(this.id);
                paragraph.content =
                    typeof this["validateContent_" + paragraph.type] !==
                    "undefined"
                        ? this["validateContent_" + paragraph.type](content)
                        : content;

                this.$store.commit("editParagraph", paragraph);
                this.$root.orderStoreIDs("script");
            }
        },

        /**
         * Validating character paragraphs
         *
         * @param {String} input
         */
        validateContent_character: function(input) {
            let paragraph = this.$store.getters.getParagraphById(this.id),
                relCharacter = this.$store.getters.getCharacterByName(input);

            if (!relCharacter) {
                let newCharacter = {
                    name: input,
                    description: "",
                    bio: "",
                    info: {
                        Age: "",
                        Height: "",
                        Weight: "",
                        Hair: "",
                        Eyes: ""
                    },
                    role: "",
                    actor: ""
                };

                this.$store.commit("addNewCharacter", newCharacter);
                relCharacter = this.$store.getters.getCharacterByName(input);
            }

            paragraph.rel.character = [relCharacter.id];
            this.$store.commit("editParagraph", paragraph);

            return input;
        },

        /**
         * Deletes specified paragraph
         *
         * @param {Number} id
         */
        deleteParagraph: function(id) {
            this.$store.commit("removeParagraph", id);
            this.$root.orderStoreIDs("script");
        },

        /**
         * initialized autocomplete in paragraphs
         */
        initAutocomplete: function() {
            if (!!this.autocompleteList && this.autocompleteList.length > 0) {
                let callback = newVal => {
                    this.content = newVal;
                };

                this.autocomplete = this.$root.autocomplete(
                    this.$el.querySelector("[data-autocomplete]"),
                    this.autocompleteList,
                    callback
                );
            }
        },

        /**
         * Returns the line count of a paragraph
         */
        calculateLines: function() {
            const zeroWidth = 8.1; // absolute width of the character "0"
            let lines = 1,
                wordList = this.content.split(" ").map(x => x.length),
                lineLength = -1,
                paragraphLength = 60;

            paragraphLength = Math.min(
                Math.floor(this.$el.clientWidth / zeroWidth - 1),
                this.paragraphTypes.filter(type => {
                    return type.name === this.type;
                })[0].characters
            );

            for (let i = 0; i <= wordList.length; i++) {
                if (lineLength >= paragraphLength + 1) {
                    lines++;
                    i--; // this word goes to the next line, so we decrement its counter
                    lineLength = 0;
                }

                lineLength += wordList[i] + 1; // +1 to compensate the whitespace that we use as a word separator
            }

            this.lines = lines;
        }
    },

    computed: {
        /**
         * returns the autocomplete options for character paragraphs
         */
        autocompleteList() {
            if (this.type === "character") {
                let characters = [];

                for (
                    let i = 0;
                    i < this.$store.state.project.character.length;
                    i++
                ) {
                    if (this.$store.getters.getCharacterById(i)) {
                        characters.push(
                            this.$store.getters.getCharacterById(i).name
                        );
                    }
                }

                return characters;
            }
        },

        type: {
            /**
             * gets the current paragraph type
             */
            get: function() {
                return this.$store.getters.getParagraphById(this.id).type;
            },

            /**
             * validates and sets the paragraph type
             *
             * @param {String} type
             */
            set: function(type) {
                let paragraph = this.$store.getters.getParagraphById(this.id);
                paragraph.type = type;

                this.$store.commit("editParagraph", paragraph);
                this.type = type;
            }
        },

        content: {
            /**
             * gets current paragraph content
             */
            get: function() {
                return this.contentProp;
            },

            /**
             * Validates and sets paragraph content
             *
             * @param {String} content
             */
            set: function(content) {
                let paragraph = this.$store.getters.getParagraphById(this.id);
                paragraph.content = content;

                this.$store.commit("editParagraph", paragraph);
            }
        },

        scene: {
            /**
             * returns the scene this paragraphs belongs to
             */
            get: function() {
                if (this.type === "scene") {
                    return this.$store.getters.getSceneNumberByParagraphId(
                        this.id
                    );
                } else {
                    return this.$store.getters.getSceneNumberByParagraphId(
                        this.$store.getters.getSceneByParagraphId(this.id)
                    );
                }
            }
        },

        character: {
            /**
             * returns the character to the current character-related paragraph
             */
            get: function() {
                let character = false;
                if (
                    this.type === "character" ||
                    this.type === "dialog" ||
                    this.type === "parenthesis"
                ) {
                    character = this.$store.getters.getRelatedCharacterByParagraph(
                        this.id
                    );
                }

                return character;
            }
        },

        rel: {
            /**
             * gets related items
             */
            get: function() {
                return this.relProp;
            }
        }
    },

    watch: {
        content: function(newVal, oldVal) {
            this.calculateLines();
        }
    },

    mounted() {
        this.calculateLines();
        this.$nextTick(() => {
            this.initAutocomplete();
        });
        window.addEventListener("resize", this.calculateLines);
    },

    beforeDestroy() {
        window.removeEventListener("resize", this.calculateLines);
    }
};
