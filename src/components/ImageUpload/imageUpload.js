export default {
    data: () => {
        return {
            currentImage: {
                type: String,
                default: ""
            },
            calculatedWidth: Number,
            calculatedHeight: Number,
            fileName: String
        };
    },

    props: {
        thumbnail: {
            type: Boolean,
            default: true
        },

        quality: {
            type: Number,
            default: 0.6
        },

        width: Number,
        height: Number
    },

    computed: {
        newWidth: {
            get: function() {
                return this.calculatedWidth || this.width;
            },
            set: function(value) {
                this.calculatedWidth = value;
            }
        },
        newHeight: {
            get: function() {
                return this.calculatedHeight || this.height;
            },
            set: function(value) {
                this.calculatedHeight = value;
            }
        }
    },

    methods: {
        /**
         * Registers click on button and triggers input
         */
        onTriggerClick: function() {
            this.$el.querySelector(".image-upload__input").click();
        },

        /**
         * Determines image file from upload event,
         * triggers conversion
         *
         * @param {Object} event
         */
        onImageUpload: function(event) {
            const img = new Image();
            img.addEventListener("load", event => {
                this.convertToJpeg(event.srcElement);
            });

            // trigger event listener
            img.src = URL.createObjectURL(event.target.files[0]);

            // file name without original ending
            this.fileName = event.target.files[0].name
                .split(".")
                .slice(0, -1)
                .join(".");
        },

        /**
         * resizes the image and converts to jpeg
         *
         * @param {Object} img <img> object
         */
        convertToJpeg: function(img) {
            this.calculateImageSize(img.width, img.height);

            const $canvas = document.createElement("canvas"),
                ctx = $canvas.getContext("2d");
            let jpeg = "";

            URL.revokeObjectURL(img.src); // free up memory from image set in onImageUpload
            $canvas.width = this.newWidth;
            $canvas.height = this.newHeight;
            ctx.drawImage(
                img, // source
                0,
                0,
                img.width,
                img.height,
                0, // destination
                0,
                this.newWidth,
                this.newHeight
            );

            // convert to JPEG
            // todo: refac into own method
            jpeg = $canvas.toDataURL("image/jpeg", this.quality);
            this.currentImage = jpeg;

            this.$emit("upload", {
                name: this.fileName,
                file: jpeg
            });
        },

        /**
         * calculates the image sizes while keeping the aspect ratio
         *
         * @param {Number} x image max width
         * @param {Number} y image max height
         */
        calculateImageSize: function(x, y) {
            const aspectRatio = x / y;

            // todo: use max width, not absolute values

            if (!this.width && !this.height) {
                this.newWidth = x;
                this.newHeight = y;
                return;
            }

            if (!this.width && !!this.height) {
                this.newHeight = this.height;
                this.newWidth = this.height * aspectRatio;
                return;
            }

            if (!this.height && !!this.width) {
                this.newWidth = this.width;
                this.newHeight = this.width / aspectRatio;
                return;
            }
        }
    }
};
