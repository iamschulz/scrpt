export default {
    data: function() {
        return {
            headlines: [
                "Animate",
                "Build",
                "Construct",
                "Edit",
                "Enhance",
                "Film",
                "Manage",
                "Outline",
                "Plan",
                "Produce",
                "Rehearse",
                "Review",
                "Show",
                "Feature",
                "Share",
                "Screen",
                "Tell",
                "Write"
            ],
            headline: "Write",
            animatedHeadline: "",
            typeSpeed: 80,
            animationInterval: 0,
            cycleSpeed: 3000,
            cycleInterval: 0
        };
    },

    methods: {
        updateHeadline: function() {
            const intervalTimeout =
                this.typeSpeed * this.headline.length + this.typeSpeed;

            this.headline = this.headlines[
                Math.floor(Math.random() * this.headlines.length)
            ];

            this.animationInterval = window.setInterval(
                this.shortenHeadline,
                this.typeSpeed
            );

            window.setTimeout(() => {
                window.clearInterval(this.animationInterval);
                this.animationInterval = window.setInterval(
                    this.buildHeadline,
                    this.typeSpeed
                );
            }, intervalTimeout);

            window.setTimeout(() => {
                window.clearInterval(this.animationInterval);
                this.animatedHeadline = this.headline;
            }, intervalTimeout + this.headline.length * this.typeSpeed);
        },

        shortenHeadline: function() {
            if (this.animatedHeadline.length > 0) {
                window.requestAnimationFrame(() => {
                    this.animatedHeadline = this.animatedHeadline.substr(
                        0,
                        this.animatedHeadline.length - 1
                    );
                });
            }
        },

        buildHeadline: function() {
            if (this.animatedHeadline.length < this.headline.length) {
                window.requestAnimationFrame(() => {
                    this.animatedHeadline += this.headline.charAt(
                        this.animatedHeadline.length
                    );
                });
            }
        }
    },

    mounted: function() {
        this.animatedHeadline = this.headline;
        this.cycleInterval = window.setInterval(
            this.updateHeadline,
            this.cycleSpeed
        );
    },

    beforeDestroy: function() {
        window.clearInterval(this.animationInterval);
        window.clearInterval(this.cycleInterval);
    }
};
