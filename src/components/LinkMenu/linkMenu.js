import CharacterButton from "../CharacterButton/CharacterButton.vue";

export default {
    components: {
        "character-button": CharacterButton
    },

    data() {
        return {
            types: [
                {
                    name: "character",
                    icon: "person",
                    active: "false"
                },
                {
                    name: "location",
                    icon: "landscape",
                    active: "false"
                }
            ]
        };
    },

    computed: {
        /**
         * Returns an Array of related items
         */
        rel() {
            let list = [];

            for (let type of this.types) {
                if (
                    this.$store.state.project[this.store][this.id].rel[
                        type.name
                    ]
                ) {
                    let relsObj = this.$store.state.project[this.store][this.id]
                            .rel[type.name],
                        rels =
                            typeof relsObj.id !== "undefined"
                                ? [relsObj.id]
                                : relsObj;

                    for (let rel of rels) {
                        list.push({
                            type: type.name,
                            id: rel
                        });
                    }
                }
            }

            return list;
        }
    },

    methods: {
        /**
         * Creates and deletes relations from paragraphs to items.
         *
         * @param {Number} paragraphId - ID of this paragraph
         * @param {String} type - Type of the related item
         * @param {String} itemId - ID of the related item
         */
        toggleLink: function(paragraphId, type, itemId) {
            let paragraph = this.$store.getters.getParagraphById(paragraphId);

            // create relations object if not existant
            paragraph.rel[type] = paragraph.rel[type] || [];

            // if relation already exists, delete it
            if (paragraph.rel[type].indexOf(itemId) >= 0) {
                let rel =
                    typeof paragraph.rel[type].id !== "undefined"
                        ? [paragraph.rel[type].id]
                        : paragraph.rel[type];
                paragraph.rel[type].splice(rel.indexOf(itemId), 1);
            } else {
                paragraph.rel[type].push(itemId);
            }

            this.$store.commit("editParagraph", paragraph);
        },

        /**
         * Checks if an item is related to a paragraph
         *
         * @param {Number} paragraphId - ID of this paragraph
         * @param {String} type - Type of the related item
         * @param {String} itemId - ID of the related item
         */
        isRelInParagraph: function(paragraphId, type, itemId) {
            if (!this.$store.state.project.script[paragraphId].rel[type]) {
                return false;
            }

            let relObj = this.$store.state.project.script[paragraphId].rel[
                    type
                ],
                rel = typeof relObj.id !== "undefined" ? [relObj.id] : relObj;

            return rel.indexOf(itemId) >= 0;
        }
    },

    props: {
        id: Number,
        store: String
    }
};
