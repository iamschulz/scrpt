import pagemap from "pagemap/dist/pagemap.min.js";

export default {
    methods: {
        /**
         * initialize minimap
         */
        initMap: function() {
            pagemap(document.querySelector(".minimap"), {
                viewport: null,
                styles: {
                    ".paragraph": "rgba(0, 0, 0, 0.1)",
                    ".paragraph__input": "rgba(0, 0, 0, 0.1)",
                    ".paragraph.is--focus .paragraph__input":
                        "rgba(239, 40, 156, 0.5)",
                    ".mdl-layout__header, .mdl-layout__header-title":
                        "rgba(0,0,0,0)"
                },
                back: "transparent",
                view: "rgba(0, 0, 0, 0.1)",
                drag: "rgba(0, 0, 0, 0.2)",
                interval: 1
            });
        }
    },

    mounted() {
        this.initMap();
    }
};
