import Paragraph from "../Paragraph/Paragraph.vue";
import Minimap from "../Minimap/Minimap.vue";

export default {
    components: {
        Paragraph: Paragraph,
        Minimap: Minimap
    },

    computed: {
        script: {
            get: function() {
                return this.$store.state.project.script;
            }
        }
    },

    methods: {
        /**
         * creates a new paragraph in the script
         * determines new ID
         * determines type based on previous paragraph
         * adds it to the store
         *
         * @param id
         */
        onEditNewParagraph: function(id) {
            let script = this.$store.state.project.script,
                paragraph = script[id],
                type = "scene",
                newParagraphPayload;

            // if the paragraph you're coming from is already emtpy
            if (script.length > 0) {
                if (
                    (paragraph && paragraph.content.length <= 0) ||
                    script[script.length - 1].content.length <= 0
                ) {
                    return false;
                }

                switch (script[script.length - 1].type) {
                    case "scene":
                        type = "action";
                        break;
                    case "character":
                        type = "dialog";
                        break;
                    case "parenthesis":
                        type = "dialog";
                        break;
                    case "dialog":
                        type = "character";
                        break;
                    case "shot":
                        type = "scene";
                        break;
                    default:
                        type = "action";
                        break;
                }
            }

            newParagraphPayload = {
                id: id + 1,
                type: type,
                content: "",
                rel: {}
            };

            this.$store.commit("addNewParagraph", newParagraphPayload);

            this.$root.orderStoreIDs("script");

            this.$nextTick(() => {
                this.focusParagraph(this, id + 1);
            });
        },

        /**
         * Focuses a given paragraph or the prev or next one
         *
         * @param event
         * @param id
         */
        focusParagraph: function(event, id) {
            // determine the paragraph to focus when ID is not given
            if (!id) {
                if (event.key === "ArrowUp") {
                    id = parseInt(event.target.dataset.id) - 1;
                } else if (event.key === "ArrowDown") {
                    id = parseInt(event.target.dataset.id) + 1;
                } else {
                    id = this.$store.state.project.script.length - 1;
                }
            }

            if (id < 0) {
                id = 0;
            } else if (id >= this.$store.state.project.script.length) {
                id = this.$store.state.project.script.length - 1;
            }

            this.$el.querySelectorAll(".paragraph__input")[id].focus();

            window.scrollY =
                document.querySelectorAll(".paragraph")[id].offsetTop - 200;
        }
    }
};
