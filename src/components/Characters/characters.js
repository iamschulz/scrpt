import CharacterButton from "../CharacterButton/CharacterButton.vue";
import SafeDeleteItem from "../SafeDeleteItem/SafeDeleteItem.vue";
import Gallery from "../Gallery/Gallery.vue";

export default {
    components: {
        "character-button": CharacterButton,
        "safe-delete-item": SafeDeleteItem,
        gallery: Gallery
    },

    data: function() {
        return {
            activeCharacter: {
                type: Number,
                default: 0
            },
            characterProps: ["age", "height", "weight", "hair", "eyes"],
            menuActive: {
                type: Boolean,
                default: false
            },
            galleryActiveImage: {
                type: Object,
                default: null
            }
        };
    },

    computed: {
        character: {
            /**
             * Gets the active character.
             */
            get: function() {
                return this.$store.state.project.character[
                    this.activeCharacter
                ];
            }
        }
    },

    created() {
        if (!isNaN(this.$route.params.id)) {
            this.activateCharacter(parseInt(this.$route.params.id));
        }
    },

    beforeRouteUpdate(to, from, next) {
        if (!isNaN(to.params.id)) {
            this.activateCharacter(parseInt(to.params.id));
        }
        next();
    },

    methods: {
        /**
         * Adds a new character to the store.
         * Focuses character view on it.
         */
        addNewCharacter: function() {
            let newCharacter = {
                name: "",
                avatar: "",
                notes: "",
                description: "",
                bio: "",
                age: "",
                height: "",
                weight: "",
                hair: "",
                eyes: "",
                role: "",
                actor: "",
                images: []
            };

            this.$store.commit("addNewCharacter", newCharacter);
            this.activateCharacter(
                this.$store.state.project.character.length - 1
            );

            this.$nextTick(() => {
                this.$el
                    .querySelector(".characters__menu-edit-name input")
                    .focus();
            });
        },

        /**
         * Updates a propery of the currently active character
         *
         * @param {String} prop - Property name to be updated
         * @param {String} val - New Value
         */
        updateCharacter: function(prop, val) {
            if (!prop) {
                throw new Error("Property is empty");
            }

            let character = this.character;
            character[prop] = val;

            this.$store.commit("editCharacter", character);
        },

        /**
         * Updates the currently active character with a new image
         *
         * @param {Object} image - needs to be {name: "foo", file: "base64"}
         */
        onImageUpload: function(image) {
            // todo: check for duplicate file names

            if (!this.character.images) {
                this.character.images = [];
            }

            let newImagesArr = this.character.images;
            newImagesArr.push(image);
            this.updateCharacter("images", newImagesArr);

            // set first image as avatar by default
            if (this.character.images - length === 0) {
                this.updateCharacter("avatar", image.name);
            }
        },

        /**
         * Deletes a specified image,
         * unlinks it from avatar, if necessary
         *
         * @param {String} imageName
         */
        deleteImage: function(imageName) {
            if (this.character.avatar === imageName) {
                this.updateCharacter("avatar", "");
            }

            let images = this.character.images;
            images.splice(
                images.findIndex(images => images.name === imageName),
                1
            );

            this.updateCharacter("images", images);
        },

        /**
         * Focus character view on specified character
         *
         * @param {Number} id
         */
        activateCharacter: function(id) {
            this.activeCharacter = id;
        }
    }
};
