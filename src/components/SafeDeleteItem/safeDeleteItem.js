export default {
    props: {
        type: String,
        id: Number
    },

    computed: {
        item: {
            /**
             * gets the item to be deleted
             */
            get: function() {
                return this.$store.state.project[this.type][this.id];
            }
        },

        relations: {
            /**
             * gets the scenes with relations to this item
             */
            get: function() {
                let rels = [],
                    relScenes = [];

                for (let paragraph of this.$store.state.project.script) {
                    if (this.itemIsRelated(paragraph)) {
                        rels.push(paragraph.id);
                        let sceneNumber = this.getSceneNumber(paragraph);

                        if (relScenes.indexOf(sceneNumber) < 0) {
                            relScenes.push(sceneNumber);
                        }
                    }
                }

                return relScenes;
            }
        },

        occurences: {
            /**
             * gets the scenes with mentions of this item
             */
            get: function() {
                let occurences = [],
                    occurenceScenes = [];

                // item name needs to be longer than 2 in order to search text for it
                if (
                    this.$store.state.project[this.type][this.id].name.length >=
                    3
                ) {
                    for (let paragraph of this.$store.state.project.script) {
                        if (this.itemOccurs(paragraph) === true) {
                            occurences.push(paragraph.id);
                            let sceneNumber = this.getSceneNumber(paragraph);

                            if (occurenceScenes.indexOf(sceneNumber) < 0) {
                                occurenceScenes.push(sceneNumber);
                            }
                        }
                    }
                }
                return occurenceScenes;
            }
        }
    },

    methods: {
        /**
         * Checks if the item is related to a speicifed paragraph
         *
         * @param {Object} paragraph
         */
        itemIsRelated: function(paragraph) {
            return (
                paragraph.rel[this.type] &&
                paragraph.rel[this.type].indexOf(this.id) >= 0
            );
        },

        /**
         * Checks if the item is mentioned in a speicifed paragraph
         *
         * @param {Object} paragraph
         */
        itemOccurs: function(paragraph) {
            return (
                paragraph.content
                    .toLowerCase()
                    .indexOf(
                        this.$store.state.project[this.type][
                            this.id
                        ].name.toLowerCase()
                    ) >= 0
            );
        },

        /**
         * gets the scene number of a paragraph
         *
         * @param {Object} paragraph
         */
        getSceneNumber: function(paragraph) {
            let sceneId = this.$store.getters.getSceneByParagraphId(
                    paragraph.id
                ),
                sceneNumber = this.$store.getters.getSceneNumberByParagraphId(
                    sceneId
                );

            return sceneNumber;
        },

        /**
         * Deletes the item
         * removes all relations to it
         * emits beforeDelete
         * closes modal
         */
        deleteItem: function() {
            let deleteMutation =
                "remove" +
                this.type.charAt(0).toUpperCase() +
                this.type.slice(1);

            if (this.relations.length > 0) {
                this.removeRelations();
            }

            this.$emit("beforeDelete");
            this.$store.commit(deleteMutation, this.id);
            this.$root.orderStoreIDs(this.type);
            this.$store.commit("toggleModal", false);
        },

        /**
         * removes all relations of the item in all paragraphs
         */
        removeRelations: function() {
            for (let paragraph of this.$store.state.project.script) {
                if (this.itemIsRelated(paragraph)) {
                    let rel =
                        typeof paragraph.rel[this.type].id !== "undefined"
                            ? [paragraph.rel[this.type].id]
                            : paragraph.rel[this.type];
                    paragraph.rel[this.type].splice(rel.indexOf(this.id), 1);

                    this.$store.commit("editParagraph", paragraph);
                }
            }
        }
    }
};
