export default {
    computed: {
        projectTitle: {
            get() {
                return this.$store.state.project.project.title;
            },
            set(value) {
                this.$store.commit("editProjectTitle", value);
            }
        },
        projectSynopsis: {
            get() {
                return this.$store.state.project.project.synopsis;
            },
            set(value) {
                this.$store.commit("editProjectSynopsis", value);
            }
        },
        projectAuthorName: {
            get() {
                return this.$store.state.project.project.author;
            },
            set(value) {
                this.$store.commit("editProjectAuthorName", value);
            }
        },
        projectAuthorAddress: {
            get() {
                return this.$store.state.project.project.address;
            },
            set(value) {
                this.$store.commit("editProjectAuthorAddress", value);
            }
        },
        projectAuthorMail: {
            get() {
                return this.$store.state.project.project.mail;
            },
            set(value) {
                this.$store.commit("editProjectAuthorMail", value);
            }
        },
        projectAuthorPhone: {
            get() {
                return this.$store.state.project.project.phone;
            },
            set(value) {
                this.$store.commit("editProjectAuthorPhone", value);
            }
        }
    }
};
