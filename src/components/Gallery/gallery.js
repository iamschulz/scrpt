import ImageUpload from "../ImageUpload/ImageUpload.vue";

export default {
    components: {
        "image-upload": ImageUpload
    },

    props: {
        images: Array,
        enableUpload: {
            type: Boolean,
            default: false
        },
        uploadWidth: {
            type: Number,
            default: 900
        },
        uploadQuality: {
            type: Number,
            default: 0.5
        }
    },

    watch: {
        images: function(value) {
            if (!value || value.length < 1) {
                this.galleryModal = false;
            }
        }
    },

    data: function() {
        return {
            galleryIndex: 0,
            galleryModal: false
        };
    },

    methods: {
        onImageUpload: function($event) {
            this.$emit("upload", {
                name: $event.name,
                file: $event.file
            });
        },

        onGalleryChange: function(newIndex) {
            if (!this.images || this.images.length < 1) {
                return;
            }

            this.galleryIndex = newIndex;
            this.$emit("galleryChange", {
                index: newIndex,
                name: this.images[newIndex].name
            });
        }
    },

    mounted: function() {
        this.onGalleryChange(0);
    }
};
