export default {
    props: {
        id: Number,
        size: {
            type: String,
            default: "32px"
        }
    },

    computed: {
        character: function() {
            return this.$store.state.project.character[this.id];
        },
        characterImage: function() {
            let characterImage = this.character.images.filter(image => {
                return image.name === this.character.avatar;
            })[0];

            return characterImage ? characterImage.file : false;
        }
    }
};
