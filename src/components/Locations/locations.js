import SafeDeleteItem from "../SafeDeleteItem/SafeDeleteItem.vue";
import Gallery from "../Gallery/Gallery.vue";

export default {
    components: {
        "safe-delete-item": SafeDeleteItem,
        gallery: Gallery
    },

    data: function() {
        return {
            locationProps: ["energy", "water", "sanitary"],
            activeLocation: {
                type: Number,
                default: 0
            },
            menuActive: {
                type: Boolean,
                default: false
            },
            galleryActiveImage: {
                type: Object,
                default: null
            }
        };
    },

    computed: {
        location: {
            /**
             * Gets the active character.
             */
            get: function() {
                return this.$store.state.project.location[this.activeLocation];
            }
        }
    },

    created() {
        if (!isNaN(this.$route.params.id)) {
            this.activateLocation(parseInt(this.$route.params.id));
        }
    },

    beforeRouteUpdate(to, from, next) {
        if (!isNaN(to.params.id)) {
            this.activateLocation(parseInt(to.params.id));
        }
        next();
    },

    methods: {
        /**
         * Adds a new location to the store.
         * Focuses location view on it.
         */
        addNewLocation: function() {
            let newLocation = {
                name: "",
                energy: "",
                water: "",
                sanitary: "",
                notes: "",
                images: []
            };

            this.$store.commit("addNewLocation", newLocation);
            this.activateLocation(
                this.$store.state.project.location.length - 1
            );

            this.$nextTick(() => {
                this.$el
                    .querySelector(".locations__menu-edit-name input")
                    .focus();
            });
        },

        /**
         * Updates a propery of the currently active location
         *
         * @param {String} prop - Property name to be updated
         * @param {String} val - New Value
         */
        updateLocation: function(prop, val) {
            if (!prop) {
                throw new Error("Property is empty");
            }

            let location = this.location;
            location[prop] = val;

            this.$store.commit("editLocation", location);
        },

        /**
         * Updates the currently active location with a new image
         *
         * @param {Object} image - needs to be {name: "foo", file: "base64"}
         */
        onImageUpload: function(image) {
            // todo: check for duplicate file names

            if (!this.location.images) {
                this.location.images = [];
            }

            let newImagesArr = this.location.images;
            newImagesArr.push(image);
            this.updateLocation("images", newImagesArr);
        },

        /**
         * Deletes a specified image,
         * unlinks it from avatar, if necessary
         *
         * @param {String} imageName
         */
        deleteImage: function(imageName) {
            let images = this.location.images;
            images.splice(
                images.findIndex(images => images.name === imageName),
                1
            );

            this.updateLocation("images", images);
        },

        /**
         * Focus location view on specified location
         *
         * @param {Number} id
         */
        activateLocation: function(id) {
            this.activeLocation = id;
        }
    }
};
