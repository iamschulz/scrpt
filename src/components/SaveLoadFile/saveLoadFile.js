export default {
    data: function() {
        return {
            projectJson: String
        };
    },

    methods: {
        /**
         * Creates a new project by clearing the store
         */
        onClickNewFile: function() {
            this.clearProject();
        },

        /**
         * Reads content of project file and triggers loading to store
         *
         * @param {Event} event
         */
        onFileChange: function(event) {
            if (event.target.files) {
                let file = event.target.files[0];
                var reader = new FileReader();
                reader.onload = event => {
                    this.importFile(event.target.result);
                };
                reader.readAsText(file);
            }
        },

        /**
         * Clear store and storage of old project
         */
        clearProject: function() {
            this.$store.state.project.script = [];
            this.$store.state.project.location = [];
            this.$store.state.project.character = [];
            this.$store.commit("editProjectTitle", "");
            this.$store.commit("editProjectSynopsis", "");
            this.$store.commit("editProjectAuthorName", "");
            this.$store.commit("editProjectAuthorMail", "");
            this.$store.commit("editProjectAuthorPhone", "");

            window.localStorage.scrpt = "";
        },

        /**
         * Import new project from file
         *
         * @param {String} projectJsonStr
         */
        importFile: function(projectJsonStr) {
            this.clearProject();

            let project = JSON.parse(projectJsonStr);

            // todo: validate file structure

            this.importProjectSettings(project);
            this.importProjectLocations(project);
            this.importProjectCharacters(project);
            this.importProjectScript(project);
        },

        /**
         * imports project settings to store
         *
         * @param {Object} project
         */
        importProjectSettings: function(project) {
            this.$store.commit("editProjectTitle", project.project.title);
            this.$store.commit("editProjectSynopsis", project.project.synopsis);
            this.$store.commit("editProjectAuthorName", project.project.author);
            this.$store.commit("editProjectAuthorMail", project.project.mail);
            this.$store.commit("editProjectAuthorPhone", project.project.phone);
        },

        /**
         * imports locations to store
         *
         * @param {Object} project
         */
        importProjectLocations: function(project) {
            for (let location of project.location) {
                this.$store.commit("addNewLocation", location);
            }
        },

        /**
         * imports characters to store
         *
         * @param {Object} project
         */
        importProjectCharacters: function(project) {
            for (let character of project.character) {
                this.$store.commit("addNewCharacter", character);
            }
        },

        /**
         * imports script to store
         *
         * @param {Object} project
         */
        importProjectScript: function(project) {
            for (let paragraph of project.script) {
                this.$store.commit("addNewParagraph", paragraph);
            }
        },

        /**
         * Generates a project file
         */
        generateFile: function() {
            let project = new Object();

            project.project = {
                title: this.$store.state.project.project.title,
                synopsis: this.$store.state.project.project.synopsis,
                author: this.$store.state.project.project.author,
                mail: this.$store.state.project.project.mail,
                phone: this.$store.state.project.project.phone
            };

            project.location = [];
            project.location = this.$store.state.project.location;

            project.character = [];
            project.character = this.$store.state.project.character;

            project.script = [];
            project.script = this.$store.state.project.script;

            // todo: validate file structure

            return JSON.stringify(project);
        },

        /**
         * Downloads the project file
         */
        onClickDownload: function() {
            this.projectJson = this.generateFile();

            this.$nextTick(() => {
                document.querySelector(".save-load__download").click();
                this.projectJson = "";
            });
        }
    }
};
