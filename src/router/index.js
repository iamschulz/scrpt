import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home/Home.vue";
import Edit from "@/components/Edit/Edit.vue";
import Locations from "@/components/Locations/Locations.vue";
import Characters from "@/components/Characters/Characters.vue";
import Project from "@/components/Project/Project.vue";
import Help from "@/components/Help/Help.vue";
import Error from "@/components/Error/Error.vue";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: "/",
            name: "home",
            component: Home,
            meta: {
                title: "Hello!"
            }
        },
        {
            path: "/edit",
            name: "edit",
            component: Edit,
            meta: {
                title: "Edit"
            }
        },
        {
            path: "/locations/:id?",
            name: "locations",
            component: Locations,
            meta: {
                title: "Locations"
            }
        },
        {
            path: "/characters/:id?",
            name: "characters",
            component: Characters,
            meta: {
                title: "Characters"
            }
        },
        {
            path: "/project",
            name: "project",
            component: Project,
            meta: {
                title: "Project"
            }
        },
        {
            path: "/help",
            name: "help",
            component: Help,
            meta: {
                title: "Help"
            }
        },
        {
            path: "*",
            name: "error",
            component: Error,
            meta: {
                title: "Error"
            }
        }
    ],
    linkExactActiveClass: "is--active",
    mode: "history",
    base: "/"
});
