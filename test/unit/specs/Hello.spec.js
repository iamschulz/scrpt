import Vue from "vue";
import Home from "@/components/Home";

describe("Home.vue", () => {
    it("should render correct contents", () => {
        const Constructor = Vue.extend(Home),
            vm = new Constructor().$mount();
        expect(vm.$el.querySelector(".home__title-text").textContent).to.equal(
            "Welcome to Your Vue.js PWA"
        );
    });
});
