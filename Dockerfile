FROM node:lts-alpine

# install sample http server for serving static content
RUN yarn global add http-server

# cwd into 'app'
WORKDIR /app

# add to PATH
ENV PATH /app/node_modules/.bin:$PATH

# copy both 'package.json' and 'yarn.lock'
COPY package.json ./
COPY yarn.lock ./

# install project dependencies
RUN yarn install

# copy project files to cwd
COPY . .

# build app for production with minification
RUN yarn build

EXPOSE 8080
CMD ["yarn", "dev"]